Zimmermann was founded by sisters Nicky and Simone Zimmermann in Sydney, Australia. The Ready-to-Wear and Swim & Resort collections embrace relaxed femininity, optimism, and effortless sophistication.

Address: 23A Bruton Street, Mayfair, London W1J 6QG, UK

Phone: +44 20 7952 2710
